from .conftest import Server
from requests import get


def test_redirect():
	def app_wrapper():
		from weepy import ASGI, Route
		app = ASGI(content_type="application/json")

		@Route("/target", content_type="text/plain")
		async def target_get(req, resp):
			return "TEXT_TARGET"

		@Route("/redirect", content_type="text/plain")
		async def redirect(req, resp):
			await resp.redirect("/target")

		return app

	server = Server(app_wrapper)

	response = get("http://localhost:3456/redirect")
	assert response.headers["Content-Type"][0:10] == "text/plain"
	assert response.status_code in [200, 301]
	assert response.text == "TEXT_TARGET"

	server.stop()
