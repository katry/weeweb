import pytest
from weepy import ASGI, Route
from re import compile


def test_init():
	test_app = ASGI()

	@Route("/")
	async def root(req, resp):
		return None

	@Route("/init-post", methods=["POST"])
	async def post(req, resp):
		return None

	@Route("/get-post", methods=["GET", "POST"])
	async def get_post(req, resp):
		return None

	@Route(compile("/getr.*"))
	async def get_r(req, resp):
		return None

	@Route(compile("/getr.*"), methods=["POST"])
	async def get_rp(req, resp):
		return None

	@Route(compile("/getr.*"), methods=["GET", "POST"])
	async def get_rgp(req, resp):
		return None
