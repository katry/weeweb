from .conftest import Server
from requests import get
from json import loads


def test_cookies():
	def app_wrapper():
		from weepy import ASGI, Route
		app = ASGI(content_type="application/json")

		@Route("/cookie", content_type="text/plain")
		async def header(req, resp):
			resp.set_cookie("test-cookie", "12345-cookie")
			return ""

		@Route("/cookie-req", content_type="text/plain")
		async def header_req(req, resp):
			jezevec = req.cookies["jezevec"]
			return "%s-%s" % (jezevec.value, jezevec["max-age"])

		return app

	server = Server(app_wrapper)

	response = get("http://localhost:3456/cookie")
	assert response.status_code == 200
	assert response.headers["Content-Type"][0:10] == "text/plain"
	assert response.text == ""
	assert response.cookies["test-cookie"] == "12345-cookie"

	response = get("http://localhost:3456/cookie-req", headers={"Cookie": "jezevec=kocka; Max-Age=345"})
	assert response.status_code == 200
	assert response.headers["Content-Type"][0:10] == "text/plain"
	assert response.text == "kocka-345"

	server.stop()
